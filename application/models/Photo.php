<?php

class Photo extends CI_Model
{

    public function __construct(){
       
        $this->load->database();
    }

    public function getPhotoCategory(){

        $query = $this->db->query('SELECT * FROM photo_category');

        return $query->result_array();

    }

    public function createPhotoCategory($name){

        $visible = 1;
        
        $sql = "INSERT INTO photo_category (Name, Visible) VALUES ('$name', $visible)";

        $this->db->query($sql);

    }

    public function updatePhotoCategory(){

    }

    public function deletePhotoCategory(){

    }

    public function getPhoto($photoCategory){

        $query = $this->db->query('SELECT * FROM photo WHERE Category_id='.$photoCategory);

        return $query->result_array();

    }

    public function createPhoto($categoryId, $path){

        $sql = "INSERT INTO photo (Category_id, Path_image) VALUES ($categoryId, '$path')";

        $this->db->query($sql);

    }

}