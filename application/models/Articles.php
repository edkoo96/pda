<?php

class Articles extends CI_Model
{

    public function __construct(){
       
        $this->load->database();
    }

    public function getArticlesCategory(){

        $query = $this->db->query('SELECT * FROM articles_category');

        return $query->result_array();

    }

    public function getArticles($id)
    {

        if(is_null($id)) $query = $this->db->query('SELECT * FROM articles');
        else $query = $this->db->query('SELECT * FROM articles WHERE Id='.$id);

        return $query->result_array();

    }

    public function createArticleCategory($nameCategory){

        $sql = "INSERT INTO articles_category (Name_category) VALUES ('$nameCategory')";

        $this->db->query($sql);

    }

    public function createArticle($category_id, $title, $content, $visible){

        $intro_image = '';
        $description = substr($content, 0, 50) . "...";
        $keywords = '';
        $date_create = date("Y-m-d");

        $sql = "INSERT INTO articles (Category_id, Title, Description, Intro_image, Content, Keywords, Date_create, Visible) VALUES ($category_id, '$title', '$description', '$intro_image', '$content', '$keywords', '$date_create', $visible)";

        $this->db->query($sql);
    }

    public function updateArticleCategory(){

    }

    public function updateArticle($content, $access, $category, $id){

        $description = substr($content, 0, 50) . "...";

        $sql = "UPDATE articles SET Content =  '$content' , Visible = $access , Category_id = $category , Description = '$description' WHERE Id = $id";

        $this->db->query($sql);
    }

    public function deleteArticleCategory(){

    }

    public function deleteArticle($id){

        $sql = "DELETE FROM articles WHERE Id = $id";

        $this->db->query($sql);

    }

}