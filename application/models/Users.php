<?php

class Users extends CI_Model
{

    public function __construct(){
       
        $this->load->database();
    }

    public function getUsers(){

        $query = $this->db->query('SELECT * FROM users');

        return $query->result_array();

    }

    public function getDetailUser($idUser){

        $query = $this->db->query('SELECT * FROM users WHERE Id='.$idUser);

        return $query->result_array();

    }

    public function createUser($name, $surname, $username, $password, $email, $level){

        $date_create = date("Y-m-d");

        $password_hash = md5($password);

        $sql = "INSERT INTO users (Name, Surname, Username, Password, Email, Level, Date_create ) VALUES ('$name', '$surname', '$username', '$password_hash', '$email', '$level', '$date_create')";

        $this->db->query($sql);
    }

    public function updateUser($name, $surname, $email, $id){

        $sql = "UPDATE users SET Name =  '$name' , Surname = '$surname' , Email = '$email' WHERE Id = $id";

        $this->db->query($sql);

    }

}