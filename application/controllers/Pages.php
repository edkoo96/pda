<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Pages extends CI_Controller {


    public function view($page = 'home')
    {
        $this->load->library('session');
        $this->load->helper('url');

        if($page!="login") {

            if ($this->session->logged == true) $this->CheckTimeLogin();
            else redirect(base_url('/login'));

        }

        if($page=="login" && $this->session->logged == true) redirect(base_url('/home'));

        switch($page){

            case "home":
                $data = $this->Home();
                break;
            case "articles":
                $data = $this->Articles();
                break;
            case "login":
                $data = $this->Login();
                break;
            case "logout":
                $this->Logout();
                break;
            case "article_create":
                $data = $this->ArticleCreate();
                break;
            case "article_edit":
                $data = $this->ArticleEdit();
                break;
            case "article_delete":
                $this->ArticleDelete();
                break;
            case "article_category":
                $data = $this->ArticleCategory();
                break;
            case "menu":
                $data = $this->Menu();
                break;
            case "menu_edit":
                $data = $this->MenuEdit();
                break;
            case "photo_category":
                $data = $this->PhotoCategory();
                break;
            case "photo_in_category":
                $data = $this->PhotoInCategory();
                break;
            case "users":
                $data = $this->Users();
                break;
            case "detail_user":
                $data = $this->DetailUser();
                break;
            case "create_user":
                $data = $this->CreateUser();
        }


        if ( ! file_exists(APPPATH. 'views/pages/'.$page.'.php'))
        {
            show_404();
        }


        $data['active_page'] = $page;

        if($page == "login") $this->load->view('templates/header-login', $data);
        else{
            $data['username'] = $this->session->username;
            $this->load->view('templates/header', $data);
        }

        $this->load->view('pages/'.$page, $data);
        $this->load->view('templates/footer', $data);
    }

    private function Home(){

        $this->load->model('Articles');
        $data['articles'] = $this->Articles->getArticles(null);

        $this->load->model('Menu');
        $data['menu_items'] = $this->Menu->getMenuItem(null);

        $this->load->model('Users');
        $data['users'] = $this->Users->getUsers();


        $data['title'] = "CMS";

        return $data;

    }

    private function Articles(){

        $this->load->model('Articles');
        $data['articles'] = $this->Articles->getArticles(null);

        $data['title'] = "CMS";

        return $data;

    }

    private function Login(){

        $this->load->library('session');
        $this->load->helper('url');

        $this->load->helper(array('form'));

        $this->load->library('form_validation');

        $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');

        $data['msg'] = null;

        if ($this->form_validation->run() == FALSE) {
           //redirect(base_url('/login'));
        }
        else {

            $this->load->model('Login');

            $username = $this->input->post('username');
            $password = $this->input->post('password');

            if ($this->Login->isAdmin($username, $password)) {
                $sess_array = array();
                $this->session->logged = true;
                $this->session->logged_time = time();
                $this->session->username = $username;
                redirect(base_url('/home'));
            } else {
                $this->session->logged = false;
                $data['msg'] = " Prihlásenie bolo neúspešné!!";
            }

        }


        $data['title'] = "CMS";

        return $data;

    }

    private function Logout(){

        $this->session->logged = false;
        redirect(base_url('/login'));
    }

    private function CheckTimeLogin(){

        if ($this->session->logged_time > 0){
            if ($this->session->logged_time<time()-600) {
                $this->session->logged = false;
                redirect(base_url('/logout'));
            }
            else
                $this->session->logged_time=time();

        }

    }

    private function ArticleCreate(){

        $this->load->model('Articles');
        $data['articles_category'] = $this->Articles->getArticlesCategory();

        $this->load->helper(array('form'));

        $this->load->library('form_validation');

        $this->form_validation->set_rules('title', 'Title', 'required');
        $this->form_validation->set_rules('category', 'Category', 'required');
        $this->form_validation->set_rules('content', 'Content', 'required');
        $this->form_validation->set_rules('access', 'Access', 'required');

        $data['msg'] = null;

        if ($this->form_validation->run() == FALSE) {
            //redirect(base_url('/login'));
        }
        else {
            
            $title = $this->input->post('title');
            $category_id = $this->input->post('category');
            $content = $this->input->post('content');

            if($this->input->post('access') == 1) $access = 1;
            else $access = 0;

            if($this->Articles->createArticle($category_id, $title, $content, $access)) $data['msg'] = 1;
            else $data['msg'] = 2;


        }
        
        $data['title'] = "CMS";
        
        return $data;
        
    }

    private function ArticleEdit(){

        $this->load->model('Articles');

        $this->load->helper(array('form'));

        $this->load->library('form_validation');

        //$this->form_validation->set_rules('title', 'Title', 'required');
        //$this->form_validation->set_rules('category', 'Category', 'required');
        $this->form_validation->set_rules('content', 'Content', 'required');
        //$this->form_validation->set_rules('access', 'Access', 'required');

        $data['msg'] = null;

        $data['title'] = "CMS ";

        if ($this->form_validation->run() == FALSE) {
            //redirect(base_url('/login'));
        }
        else {

            $category_id = $this->input->post('category');
            $content = $this->input->post('content');

            if($this->input->post('access') == 1) $access = 1;
            else $access = 0;

            $this->Articles->updateArticle($content, $access, $category_id, $this->input->get('id'));

        }

        $data['articles_category'] = $this->Articles->getArticlesCategory();
        $data['article_data'] = $this->Articles->getArticles($this->input->get('id'));
        $data['title'] = "CMS";

        return $data;

    }

    private function ArticleDelete(){

        $this->load->model('Articles');

        $this->Articles->deleteArticle($this->input->get('id'));

        redirect(base_url('/articles'));
    }

    private function ArticleCategory(){

        $this->load->model('Articles');

        $this->load->helper(array('form'));

        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('category', 'Category', 'required');

        $data['msg'] = null;

       
        if ($this->form_validation->run() == FALSE) {
            //redirect(base_url('/login'));
        }
        else {

            $category = $this->input->post('category');

            $this->Articles->createArticleCategory($category);

        }

        $data['articles_category'] = $this->Articles->getArticlesCategory();
        $data['title'] = "CMS";

        return $data;

    }

    private function MenuEdit(){

        $this->load->model('Menu');

        $this->load->helper(array('form'));

        $this->load->library('form_validation');

        $this->form_validation->set_rules('content', 'Content', 'required');

        $data['msg'] = null;


        if ($this->form_validation->run() == FALSE) {
            //redirect(base_url('/login'));
        }
        else {
            
            $content = $this->input->post('content');

            $this->Menu->updateMenuItem($content,$this->input->get('id'));

        }

        $data['menu_items'] = $this->Menu->getMenuItem($this->input->get('id'));
        $data['title'] = "CMS";

        return $data;

    }

    private function Menu(){

        $this->load->model('Menu');

        $data['menu_items'] = $this->Menu->getMenuItem(null);
        $data['title'] = "CMS";

        return $data;

    }

    private function PhotoCategory(){

        $this->load->model('Photo');

        $this->load->helper(array('form'));

        $this->load->library('form_validation');

        $this->form_validation->set_rules('category', 'Category', 'required');

        $data['msg'] = null;


        if ($this->form_validation->run() == FALSE) {
            //redirect(base_url('/login'));
        }
        else {

            $category = $this->input->post('category');

            $this->Photo->createPhotoCategory($category);

        }

        $data['photos_category'] = $this->Photo->getPhotoCategory();
        $data['title'] = "CMS";

        return $data;

    }

    private function PhotoInCategory(){

        $this->load->model('Photo');

        $this->load->helper(array('form'));

        $this->load->library('form_validation');
        
        $config['upload_path']   =   "uploads/";

        $config['allowed_types'] =   "gif|jpg|jpeg|png";

        $config['max_size']      =   "5000";

        $config['max_width']     =   "1907";

        $config['max_height']    =   "1280";

        $this->load->library('upload',$config);

        $data['msg'] = null;


        if(!$this->upload->do_upload())

        {

           // echo $this->upload->display_errors();

        }
        else {

            //upload fotografie a zapisanie do db
            $finfo=$this->upload->data();
            $filename = $finfo['file_name'];
            $this->Photo->createPhoto($this->input->get('id'), $filename);

        }

        $data['idCategory'] = $this->input->get('id');
        $data['photos'] = $this->Photo->getPhoto($this->input->get('id'));
        $data['title'] = "CMS";

        return $data;

    }
    
    private function Users(){

        $this->load->model('Users');

        $data['users'] = $this->Users->getUsers();
        $data['title'] = "CMS";

        return $data;
    }
    
    private function DetailUser(){

        $this->load->model('Users');

        $this->load->helper(array('form'));

        $this->load->library('form_validation');

        $this->form_validation->set_rules('email', 'Email', 'required');

        $data['msg'] = null;

        if ($this->form_validation->run() == FALSE) {
            //redirect(base_url('/login'));
        }
        else {

            $name = $this->input->post('name');
            $surname = $this->input->post('surname');
            $email = $this->input->post('email');
          
            $this->Users->updateUser($name, $surname, $email, $this->input->get('id'));

        }

        $data['users'] = $this->Users->getDetailUser($this->input->get('id'));
        $data['title'] = "CMS";
        
        return $data;
    }

    private function CreateUser(){

        $this->load->model('Users');

        $this->load->helper(array('form'));

        $this->load->library('form_validation');

        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('surname', 'Surname', 'required');
        $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required');


        $data['msg'] = null;

        if ($this->form_validation->run() == FALSE) {
            //redirect(base_url('/login'));
        }
        else {

            $name = $this->input->post('name');
            $surname = $this->input->post('surname');
            $username = $this->input->post('username');
            $password = $this->input->post('password');
            $email = $this->input->post('email');
            $level = $this->input->post('level');

            $this->Users->createUser($name, $surname, $username, $password, $email, $level);

            redirect(base_url('/users'));

        }

        $data['title'] = "CMS";

        return $data;
    }

}
