<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php echo $title; ?></title>

    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="assets/css/style.css">
    

    <script src="//cdn.ckeditor.com/4.5.4/full/ckeditor.js"></script>
    <!--<script src="tinymce/tinymce.min.js"></script>
    <script type="text/javascript">
        tinymce.init({
            selector: "#Content"
        });
        tinymce.init({
            language: "sk"
        });
    </script>-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body>

<div class="header">
<nav role="navigation" class="navbar">

    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
        <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="logo navbar-brand" href="#"></a>
    </div>
    <!-- Collection of nav links and other content for toggling -->
    <div class="menu">
        <div id="navbarCollapse" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li <?php if($active_page == "home") echo 'class="active"'?>><a href="home"><span class="glyphicon glyphicon-dashboard"></span></span></a></li>
                <li <?php if($active_page == "menu") echo 'class="active"'?>><a href="menu"><span class="glyphicon glyphicon-menu-hamburger icon"></span>Menu</a></li>
                <li <?php if($active_page == "articles") echo 'class="active"'?>><a href="articles"><span class="glyphicon glyphicon-list-alt icon"></span>Články</a></li>
                <li <?php if($active_page == "users") echo 'class="active"'?>><a href="users"><span class="glyphicon glyphicon-user icon"></span>Užívatelia</a></li>
                <li <?php if($active_page == "photo_category") echo 'class="active"'?>><a href="photo_category"><span class="glyphicon glyphicon-picture icon"></span>Galéria</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#"><span class="glyphicon glyphicon-cog icon"></span><b class="caret"></b></a>
                    <ul role="menu" class="dropdown-menu">
                        <li style=" border-bottom: 0px; color: black; margin-left: 20px"><span class="glyphicon glyphicon-user icon"></span><?php echo $username ?></li>
                        <li style=" border-bottom: 0px; float: center;"><a href="logout"><button type="button" class="btn btn-success navbar-btn btn-circle last">Odhlásiť sa</button></a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>
</div>