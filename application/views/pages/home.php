<div class="container">
    <div class="iconcontainer" style="margin-top: 80px">

        <div class="row">

            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div class="iconbox">
                    <div class="iconbox-icon">
                        <span class="glyphicon glyphicon-menu-hamburger"></span>
                    </div>
                    <div class="featureinfo">
                        <h4 class="text-center">Menu</h4>
                        <p>


                        <div class="span7">
                            <div class="widget stacked widget-table action-table">

                                <div class="widget-header">
                                    <i class="icon-th-list"></i>
                                    <h3>Hlavné menu</h3>
                                </div>

                                <div class="widget-content">

                                    <table class="table table-striped table-bordered">
                                        <thead>
                                        <tr>
                                            <th style="width: 8%">Id</th>
                                            <th style="width: 77%">Názov</th>
                                            <th class="td-actions">Akcia</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        <?php foreach ($menu_items as $menu_item):?>

                                        <tr>
                                <td><p style="float: left; margin-top: 15px;"><?php echo $menu_item['Id']; ?></p></td>
                                <td><p style="float: left; margin-top: 15px;"><?php echo $menu_item['Name']; ?></p></td>
                                <td class="td-actions">
                                    <a href="menu_edit?id=<?php echo $menu_item['Id']; ?>"><button type="button" class="btn btn-success navbar-btn btn-circle">Upraviť</button></a>
                                </td>
                            </tr>

                            <?php endforeach; ?>

                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>


                        </p>
                        <p style="float: left"></p><a class="btn btn-default btn-sm" href="menu" role="button">Zobraziť viac »</a>
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div class="iconbox">
                    <div class="iconbox-icon">
                        <span class="glyphicon glyphicon-list-alt"></span>
                    </div>
                    <div class="featureinfo">
                        <h4 class="text-center">Články</h4>
                        <p>
                        <div class="span7">
                            <div class="widget stacked widget-table action-table">

                                <div class="widget-header">
                                    <i class="icon-th-list"></i>
                                    <h3>Články</h3>
                                </div>

                                <div class="widget-content">

                                    <table class="table table-striped table-bordered">
                                        <thead>
                                        <tr>
                                            <th style="width: 8%">Id</th>
                                            <th style="width: 49%">Názov</th>
                                            <th style="width: 1%">P</th>
                                            <th class="td-actions">Akcia</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        <?php foreach ($articles as $article):

                                        $access = null;
                                        if($article['Visible'] == 1) $access = '<span class="glyphicon glyphicon-eye-open">';
                                        else $access = '<span class="glyphicon glyphicon-eye-close">';?>

                            <tr>
                                <td><p style="float: left; margin-top: 15px;"><?php echo $article['Id']; ?></p></td>
                                <td><p style="float: left; margin-top: 15px;"><?php echo $article['Title']; ?></p></p></td>
                                <td><p style="float: left; margin-top: 15px;"><?php echo $access; ?></span></p></td>
                                <td class="td-actions">
                                    <a href="article_edit?id=<?php echo $article['Id']; ?>"><button type="button" class="btn btn-success navbar-btn btn-circle">Upraviť</button></a>
                                    <a href="article_delete?id=<?php echo $article['Id']; ?>"><button type="button" class="btn btn-success navbar-btn btn-circle">Vymazať</button></a>
                                </td>
                            </tr>

                        <?php endforeach; ?>

                                        </tbody>
                                    </table>

                                </div>

                            </div>
                        </div>
                        </p>
                        <p style="float: left"></p><a class="btn btn-default btn-sm" href="articles" role="button">Zobraziť viac »</a>
                    </div>
                </div>
            </div>

        </div>

        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div class="iconbox">
                    <div class="iconbox-icon">
                        <span class="glyphicon glyphicon-user"></span>
                    </div>
                    <div class="featureinfo">
                        <h4 class="text-center">Užívatelia</h4>
                        <p>
                        <div class="span7">
                            <div class="widget stacked widget-table action-table">

                                <div class="widget-header">
                                    <i class="icon-th-list"></i>
                                    <h3>Užívatelia</h3>
                                </div>

                                <div class="widget-content">

                                    <table class="table table-striped table-bordered">
                                        <thead>
                                        <tr>
                                            <th style="width: 8%">Id</th>
                                            <th style="width: 65%">Meno</th>
                                            <th class="td-actions">Akcia</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        <?php foreach ($users as $user): ?>

                            <tr>
                                <td><p style="float: left; margin-top: 15px;"><?php echo $user['Id']; ?></p></td>
                                <td><p style="float: left; margin-top: 15px;"><?php echo $user['Name'] . " " .$user['Surname']; ?></p></td>
                                <td class="td-actions">
                                    <button type="button" class="btn btn-success navbar-btn btn-circle">Detail</button>
                                </td>
                            </tr>

                                        <?php endforeach; ?>

                                        </tbody>
                                    </table>

                                </div>

                            </div>
                        </div>
                        </p>
                        <a class="btn btn-default btn-sm" href="users" role="button">Zobraziť viac »</a>
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div class="iconbox">
                    <div class="iconbox-icon">
                        <span class="glyphicon glyphicon-stats"></span>
                    </div>
                    <div class="featureinfo">
                        <h4 class="text-center">Návštevnosť</h4>
                        <img src="http://robotechvision.com/images/construction_sign.png" width="100" ><p>Graf pre návštevnosť bude vyhotovený keď budem mať s čoho počítať návštevnosť.</p>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
