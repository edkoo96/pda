<div class="container">
    <div class="col-sm-12 col-md-12 col-lg-12"  style="margin-top: -30px;">
        <div class="iconbox">
            <div class="iconbox-icon">
                <span class="glyphicon glyphicon-menu-hamburger"></span>
            </div>
            <div class="featureinfo">
                <h4 class="text-center">Menu</h4>
                <p>
                    <div class="span7">

                        <div class="widget stacked widget-table action-table">

                            <div class="widget-header">
                                <i class="icon-th-list"></i>
                                <h3>Hlavné menu</h3>
                            </div>

                            <div class="widget-content">

                                <table class="table table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th style="width: 8%">Id</th>
                                        <th style="width: 87%">Názov</th>
                                        <th class="td-actions">Akcia</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    <?php foreach ($menu_items as $menu_item):?>
                                    <tr>
                                      <td><p style="float: left; margin-top: 15px;"><?php echo $menu_item['Id']; ?></p></td>
                                      <td><p style="float: left; margin-top: 15px;"><?php echo $menu_item['Name']; ?></p></td>
                                      <td class="td-actions">
                                          <a href="menu_edit?id=<?php echo $menu_item['Id']; ?>"><button type="button" class="btn btn-success navbar-btn btn-circle">Upraviť</button></a>
                                      </td>
                                    </tr>
                                    <?php endforeach; ?>
                </tbody>
                </table>

            </div>

        </div>            </div>
    </p>
</div>
</div>
</div>
</div>