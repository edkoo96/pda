<div class="container">
    <div class="col-sm-12 col-md-12 col-lg-12"  style="margin-top: -30px;">
        <div class="iconbox">
            <div class="iconbox-icon">
                <span class="glyphicon glyphicon-picture icon"></span>
            </div>
            <div class="featureinfo">
                <h4 class="text-center">Fotogaléria</h4>
                <p>
                    <div class="span7">

                        <div class="widget stacked widget-table action-table" style="margin-top: 20px">

                            <div class="widget-header">
                                <i class="icon-th-list"></i>
                                <h3>Pridať kategóriu pre fotky</h3>
                            </div>

                            <div class="widget-content">

                                <?php
                                $this->load->helper('form');
                                $this->load->library('form_validation');
                                echo form_open(base_url('photo_category'));
                                ?>

                                    <table class="table table-striped table-bordered">

                                        <tr><td><p style="float: left">Kategória:</p>
                <div class="col-md-6">
                    <div class="form-group">
                        <input type="text" class="form-control" name="category" autocomplete="off" id="Title" placeholder="Kategória" style="float: left; margin-top: 10px;">
                    </div>
                </div>
                </td></tr>
                <tr><td>
                        <div class="col-md-12">
                            <button type="submit" class="btn main-btn pull-right">Vytvoriť kategóriu</button></a>&nbsp;&nbsp;&nbsp; </form>
                        </div>
                    </td></tr>

                </table>


            </div>

        </div>

        <div class="widget stacked widget-table action-table" style="margin-top: 40px">

            <div class="widget-header">
                <i class="icon-th-list"></i>
                <h3>Kategórie fotografií</h3>
            </div>

            <div class="widget-content">

                <table class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th style="width: 8%">Id</th>
                        <th style="width: 75%">Kategória</th>
                        <th class="td-actions">Akcia</th>
                    </tr>
                    </thead>
                    <tbody>

                    <?php foreach($photos_category as $photo_category): ?>

                    <tr>
                        <td><p style="float: left; margin-top: 15px;"><?php echo $photo_category['Id']; ?></p></td>
                        <td><p style="float: left; margin-top: 15px;"><?php echo $photo_category['Name']; ?></p></td>
                        <td class="td-actions">
                            <a href=""><button type="button" class="btn btn-success navbar-btn btn-circle">Upraviť</button></a>
                            <a href="photo_in_category?id=<?php echo $photo_category['Id']; ?>"><button type="button" class="btn btn-success navbar-btn btn-circle">Otvoriť</button></a>
                        </td>
                    </tr>

                    <?php endforeach; ?>

                    </tbody>
                </table>

            </div>

        </div>

    </div>
    </p>
</div>
</div>
</div>
</div>