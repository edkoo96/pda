<div class="container">
    <div class="col-sm-12 col-md-12 col-lg-12"  style="margin-top: -30px;">
        <div class="iconbox">
            <div class="iconbox-icon">
                <span class="glyphicon glyphicon-user icon"></span>
            </div>
            <div class="featureinfo">
                <h4 class="text-center">Užívatelia</h4>
                <p>
                <div class="span7">

                    <div class="widget stacked widget-table action-table">

                        <div class="widget-header">
                            <i class="icon-th-list"></i>
                            <h3>Vytvorenie užívatela</h3>
                        </div>

                        <div class="widget-content">

                            <?php
                            $this->load->helper('form');
                            $this->load->library('form_validation');
                            echo form_open(base_url('create_user'));
                            ?>

                            <table class="table table-striped table-bordered">

                                <tr><td><p style="float: left">Meno:</p>
                <div class="col-md-5">
                    <div class="form-group">
                        <input type="text" class="form-control" name="name" autocomplete="off" id="Title" placeholder="Meno" style="float: left; margin-top: 10px;">
                    </div>
                </div>
                <p style="float: left">Priezvisko:</p>
                <div class="col-md-5">
                    <div class="form-group">
                    <input type="text" class="form-control" name="surname" autocomplete="off" id="Title" placeholder="Priezvisko" style="float: left; margin-top: 10px;">
                </div>
                </div>
                </td></tr>

                <tr><td><p style="float: left">Username:</p>
                        <div class="col-md-5">
                            <div class="form-group">
                                <input type="text" class="form-control" name="username" autocomplete="off" id="Title" placeholder="Username" style="float: left; margin-top: 10px;">
                            </div>
                        </div>
                        <p style="float: left">Heslo:</p>
                        <div class="col-md-5">
                            <div class="form-group">
                                <input type="password" class="form-control" name="password" autocomplete="off" id="Title" placeholder="Heslo" style="float: left; margin-top: 10px;">
                            </div>
                        </div>
                    </td></tr>

                <tr><td><p style="float: left">Email:</p>
                        <div class="col-md-5">
                            <div class="form-group">
                                <input type="text" class="form-control" name="email" autocomplete="off" id="Title" placeholder="Email" style="float: left; margin-top: 10px;">
                            </div>
                        </div>
                        <p style="float: left">Level:</p>
                        <div class="col-md-5">
                            <div class="form-group">
                                <select name="level" class="form-control" id="BPBABAN_SELECT" style="float: left; margin-top: 10px;">

                                    <option value="admin">admin</option>;
                                    <option value="member">member</option>;

                                </select>
                            </div>
                        </div>
                    </td></tr>

                <tr><td>
                        <div class="col-md-12">
                            <button type="submit" class="btn main-btn pull-right">Uložiť</button></a>&nbsp;&nbsp;&nbsp; </form>
                            <a href="users" class="btn main-btn pull-right">Vrátiť sa</a>
                        </div>
                    </td></tr>

                </table>

                        </div>

                    </div>

                </div>
                </p>
            </div>
        </div>
    </div>
</div>