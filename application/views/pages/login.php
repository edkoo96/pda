<div class="container">
    <div class="row"  style="margin-top: 150px">
        <div class='col-md-3'></div>
        <div class="col-md-6">
            <div class="login-box well">

                <?php
                $this->load->helper('form');
                $this->load->library('form_validation');
                echo form_open(base_url('login'));

                if($msg!=null){


                    echo '<div class="alert alert-danger" style = "text-align: left;" >
                    <a href = "#" class="close" data - dismiss = "alert" aria - label = "close" >&times;</a >
                    <strong > POZOR!</strong >'.$msg.'</div >';
                }
                ?>
                    <legend>Prihlásenie</legend>
                    <div class="form-group">
                        <label for="username">Prihlasovacie meno</label>
                        <input value='' name="username" placeholder="Username" type="text" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label for="password">Heslo</label>
                        <input name="password" value='' placeholder="Password" type="password" class="form-control" />
                    </div>
                    <div class="form-group">
                        <input id="btn_login" name="btn_login" type="submit" class="btn btn-default btn-login-submit btn-block m-t-md" value="Prihlásiť" />
                    </div>

                    <span class='text-center'><a href="" class="text-sm">Zabudnuté heslo?</a></span>
                </form>

            </div>
        </div>
        <div class='col-md-3'></div>
    </div>
</div>
