<div class="container">
    <div class="col-sm-12 col-md-12 col-lg-12"  style="margin-top: -30px;">
        <div class="iconbox">
            <div class="iconbox-icon">
                <span class="glyphicon glyphicon-picture icon"></span>
            </div>
            <div class="featureinfo">
                <h4 class="text-center">Fotogaléria</h4>
                <p>
                    <div class="span7">

                        <div class="widget stacked widget-table action-table" style="margin-top: 20px">
                            <a href="photo_category"><button type="button" class="btn btn-success navbar-btn btn-circle">Kategórie</button></a>

                            <div class="widget-header">
                                <i class="icon-th-list"></i>
                                <h3>Pridať fotku</h3>
                            </div>

                            <div class="widget-content">

                                <?php
                                $this->load->helper('form');
                                $this->load->library('form_validation');
                                echo form_open_multipart(base_url('photo_in_category?id='.$idCategory));
                                ?>

                                <table class="table table-striped table-bordered">

                                    <tr><td>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label">Select File</label>
                        <input id="input-1" type="file" class="file" name="userfile">
                    </div>
                </div>
                </td></tr>
                <tr><td>
                        <div class="col-md-12">
                            <button type="submit" class="btn main-btn pull-right">Pridaj</button></a>&nbsp;&nbsp;&nbsp; </form>
                        </div>
                    </td></tr>

                </table>


            </div>

        </div>

        <div class="widget stacked widget-table action-table" style="margin-top: 40px">

            <div class="widget-header">
                <i class="icon-th-list"></i>
                <h3>Fotografie</h3>
            </div>

            <div class="widget-content">

                <table class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th style="width: 8%">Id</th>
                        <th style="width: 75%">Fotografia</th>
                        <th class="td-actions">Akcia</th>
                    </tr>
                    </thead>
                    <tbody>

                    <?php foreach($photos as $photo): ?>

                        <tr>
                            <td><p style="float: left; margin-top: 15px;"><?php echo $photo['Id']; ?></p></td>
                            <td><p style="float: left; margin-top: 15px;"><?php echo $photo['Path_image']; ?></p></td>
                            <td class="td-actions">
                                <a href=""><button type="button" class="btn btn-success navbar-btn btn-circle">Vymazať</button></a>
                                <a href="<?php echo base_url('uploads/'.$photo['Path_image'])?>" target="_blank"><button type="button" class="btn btn-success navbar-btn btn-circle">Otvoriť</button></a>
                            </td>
                        </tr>

                    <?php endforeach; ?>

                    </tbody>
                </table>

            </div>

        </div>

    </div>
    </p>
</div>
</div>
</div>
</div>