<?php foreach ($menu_items as $data):

    $id = $data['Id'];
    $name = $data['Name'];
    $content = $data['Content'];

endforeach; ?>

<div class="container">
    <div class="col-sm-12 col-md-12 col-lg-12"  style="margin-top: -30px;">
        <div class="iconbox">
            <div class="iconbox-icon">
                <span class="glyphicon glyphicon-menu-hamburger"></span>
            </div>
            <div class="featureinfo">
                <h4 class="text-center">Menu</h4>
                <p>
                    <div class="span7">

                        <div class="widget stacked widget-table action-table">

                            <div class="widget-header">
                                <i class="icon-th-list"></i>
                                <h3>Upraviť</h3>
                            </div>

                            <div class="widget-content">

                                <?php
                                $this->load->helper('form');
                                $this->load->library('form_validation');
                                echo form_open(base_url('menu_edit?id='.$id));
                                ?>

                                    <table class="table table-striped table-bordered">

                                        <tr><td><p style="float: left">Titulok:</p>
                <div class="col-md-6">
                    <div class="form-group">
                        <input type="text" class="form-control" name="title" autocomplete="off" id="Title" placeholder="<?php echo $name; ?>" style="float: left; margin-top: 10px;" disabled>
                    </div>
                </div>
                </td></tr>
                <tr><td><p style="float: left">Obsah:</p>
                        <div class="col-md-12">
                            <div class="form-group">
                            <textarea class="form-control textarea" rows="3" name="content" id="Content" placeholder="Content" style="float: left; margin-top: 3px; min-height: 600px">
                                <?php echo $content; ?>
                            </textarea>
                                <script>
                                    CKEDITOR.replace( 'content' , { height: 600 } );
                                </script>
                            </div>
                        </div>
                    </td></tr>
                <tr><td>
                        <div class="col-md-12">
                            <button type="submit" class="btn main-btn pull-right">Uložiť zmeny</button> &nbsp;&nbsp;&nbsp; </form>
                            <a href="menu" class="btn main-btn pull-right">Vrátiť sa</a>
                        </div>
                    </td></tr>

                </table>


            </div>

        </div>
    </div>
    </p>
</div>
</div>
</div>
</div>