<?php foreach ($users as $user):

$id = $user['Id'];
$name = $user['Name'];
$surname = $user['Surname'];
$username = $user['Username'];
$email = $user['Email'];
$level = $user['Level'];
$date = $user['Date_create'];

endforeach; ?>

<div class="container">
    <div class="col-sm-12 col-md-12 col-lg-12"  style="margin-top: -30px;">
        <div class="iconbox">
            <div class="iconbox-icon">
                <span class="glyphicon glyphicon-user icon"></span>
            </div>
            <div class="featureinfo">
                <h4 class="text-center">Užívatel - <?php echo $name . " " . $surname; ?></h4>
                <p>
                <div class="span7">

                    <p>Užívatel bol vytvorený   <?php echo $date; ?></p>
                    <div class="widget stacked widget-table action-table">

                        <div class="widget-header">
                            <i class="icon-th-list"></i>
                            <h3>Detailné informácie</h3>
                        </div>

                        <div class="widget-content">

                            <?php
                            $this->load->helper('form');
                            $this->load->library('form_validation');
                            echo form_open(base_url('detail_user?id='.$id));
                            ?>

                            <table class="table table-striped table-bordered">

                                <tr><td><p style="float: left">Meno:</p>
                <div class="col-md-5">
                    <div class="form-group">
                        <input type="text" class="form-control" name="name" autocomplete="off" id="Title" value="<?php echo $name; ?>" style="float: left; margin-top: 10px;">
                    </div>
                </div>
                <p style="float: left">Priezvisko:</p>
                <div class="col-md-5">
                    <div class="form-group">
                    <input type="text" class="form-control" name="surname" autocomplete="off" id="Title" value="<?php echo $surname; ?>" style="float: left; margin-top: 10px;">
                </div>
                </div>
                </td></tr>

                <tr><td><p style="float: left">Username:</p>
                        <div class="col-md-5">
                            <div class="form-group">
                                <input type="text" class="form-control" name="username" autocomplete="off" id="Title" value="<?php echo $username; ?>" style="float: left; margin-top: 10px;" disabled>
                            </div>
                        </div>
                        <p style="float: left">Heslo:</p>
                        <div class="col-md-5">
                            <div class="form-group">
                                <p>Zmena prihlasovacieho mena a hesla iba na požiadanie hlavného administrátora!</p>
                            </div>
                        </div>
                    </td></tr>

                <tr><td><p style="float: left">Email:</p>
                        <div class="col-md-5">
                            <div class="form-group">
                                <input type="text" class="form-control" name="email" autocomplete="off" id="Title" value="<?php echo $email; ?>" style="float: left; margin-top: 10px;">
                            </div>
                        </div>
                        <p style="float: left">Level:</p>
                        <div class="col-md-5">
                            <div class="form-group">
                                <select name="level" class="form-control" id="BPBABAN_SELECT" style="float: left; margin-top: 10px;">

                                        <option value=""> <?php echo $level; ?> </option>;

                                </select>
                            </div>
                        </div>
                    </td></tr>

                <tr><td>
                        <div class="col-md-12">
                            <button type="submit" class="btn main-btn pull-right">Uložiť úpravy</button></a>&nbsp;&nbsp;&nbsp; </form>
                            <a href="users" class="btn main-btn pull-right">Vrátiť sa</a>
                        </div>
                    </td></tr>

                </table>

                        </div>

                    </div>

                </div>
                </p>
            </div>
        </div>
    </div>
</div>