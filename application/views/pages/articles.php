<div class="container">
    <div class="col-sm-12 col-md-12 col-lg-12"  style="margin-top: -30px;">
        <div class="iconbox">
            <div class="iconbox-icon">
                <span class="glyphicon glyphicon-list-alt"></span>
            </div>
            <div class="featureinfo">
                <h4 class="text-center">Články</h4>
                <p>
                <div class="span7">

                    <div class="widget stacked widget-table action-table">
                        <a href="article_category"><button type="button" class="btn btn-success navbar-btn btn-circle">Kategórie</button></a>
                        <a href="article_create"><button type="button" class="btn btn-success navbar-btn btn-circle">Pridať nový článok</button></a>

                        <div class="widget-header">
                            <i class="icon-th-list"></i>
                            <h3>Články</h3>
                        </div>

                        <div class="widget-content">

                            <table class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th style="width: 8%">Id</th>
                                    <th style="width: 67%">Názov</th>
                                    <th class="">Prístup</th>
                                    <th class="td-actions">Akcia</th>
                                </tr>
                                </thead>
                                <tbody>

                                <?php foreach ($articles as $article):

                                $access = null;
                                if($article['Visible'] == 1) $access = '<span class="glyphicon glyphicon-eye-open">';
                                else $access = '<span class="glyphicon glyphicon-eye-close">';?>

                                <tr>
                                    <td><p style="float: left; margin-top: 15px;"><?php echo $article['Id']; ?></p></td>
                                    <td><p style="float: left; margin-top: 15px;"><?php echo $article['Title']; ?></p></p></td>
                                    <td><p style="float: left; margin-top: 15px;"><?php echo $access; ?></span></p></td>
                                    <td class="td-actions">
                                        <a href="article_edit?id=<?php echo $article['Id']; ?>"><button type="button" class="btn btn-success navbar-btn btn-circle">Upraviť</button></a>
                                        <a href="article_delete?id=<?php echo $article['Id']; ?>"><button type="button" class="btn btn-success navbar-btn btn-circle">Vymazať</button></a>
                                    </td>
                                </tr>

                                <?php endforeach; ?>

                                </tbody>
                            </table>

                        </div>

                    </div>

                </div>
                </p>
            </div>
        </div>
    </div>
</div>