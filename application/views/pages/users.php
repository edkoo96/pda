<div class="container">
    <div class="col-sm-12 col-md-12 col-lg-12"  style="margin-top: -30px;">
        <div class="iconbox">
            <div class="iconbox-icon">
                <span class="glyphicon glyphicon-user icon"></span>
            </div>
            <div class="featureinfo">
                <h4 class="text-center">Užívatelia</h4>
                <p>
                <div class="span7">

                    <div class="widget stacked widget-table action-table">
                        <a href="create_user"><button type="button" class="btn btn-success navbar-btn btn-circle">Pridať nového užívateľa</button></a>

                        <div class="widget-header">
                            <i class="icon-th-list"></i>
                            <h3>Užívatelia</h3>
                        </div>

                        <div class="widget-content">

                            <table class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th style="width: 8%">Id</th>
                                    <th style="width: 37%">Meno a priezvisko</th>
                                    <th style="width: 37%">Email</th>
                                    <th class="">Level</th>
                                    <th class="td-actions">Akcia</th>
                                </tr>
                                </thead>
                                <tbody>

                                <?php foreach ($users as $user): ?>

                                <tr>
                                    <td><p style="float: left; margin-top: 15px;"><?php echo $user['Id']; ?></p></td>
                                    <td><p style="float: left; margin-top: 15px;"><?php echo $user['Name'] . " " .$user['Surname']; ?></p></p></td>
                                    <td><p style="float: left; margin-top: 15px;"><?php echo $user['Email']; ?></span></p></td>
                                    <td><p style="float: left; margin-top: 15px;"><?php echo $user['Level']; ?></span></p></td>
                                    <td class="td-actions">
                                        <a href="detail_user?id=<?php echo $user['Id']; ?>"><button type="button" class="btn btn-success navbar-btn btn-circle">Detail</button></a>
                                    </td>
                                </tr>

                                <?php endforeach; ?>

                                </tbody>
                            </table>

                        </div>

                    </div>

                </div>
                </p>
            </div>
        </div>
    </div>
</div>