<?php foreach ($article_data as $data):

    $id = $data['Id'];
    $title = $data['Title'];
    $content = $data['Content'];
    $category = $data['Category_id'];
    $access = $data['Visible'];

endforeach; ?>


<div class="container">
    <div class="col-sm-12 col-md-12 col-lg-12"  style="margin-top: -30px;">
        <div class="iconbox">
            <div class="iconbox-icon">
                <span class="glyphicon glyphicon-list-alt"></span>
            </div>
            <div class="featureinfo">
                <h4 class="text-center">Články</h4>
                <p>
                    <div class="span7">

                        <?php
                        if($msg == 1){
                            echo '<div class="alert alert-success" style="text-align: left;">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>Úspech!</strong> Stránka bola úspešne pridaná.</div>';
                        }
                        else if($msg == 2){
                            echo '<div class="alert alert-warning" style="text-align: left;">
                         <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                         <strong>Pozor chyba! Kontaktujte spravcu!</strong></div>';
                        }
                        ?>


                        <div class="widget stacked widget-table action-table">

                            <div class="widget-header">
                                <i class="icon-th-list"></i>
                                <h3>Upraviť článok</h3>
                            </div>

                            <div class="widget-content">

                        <?php
                        $this->load->helper('form');
                        $this->load->library('form_validation');
                        echo form_open(base_url('article_edit?id='.$id));
                        ?>

<table class="table table-striped table-bordered">

                                        <tr><td><p style="float: left">Titulok:</p>
                <div class="col-md-5">
                    <div class="form-group">
                        <input type="text" class="form-control" name="title" autocomplete="off" id="Title" placeholder="<?php echo $title; ?>" style="float: left; margin-top: 10px;" disabled>
                    </div>
                </div>
                <p style="float: left">Kategória:</p>
                <div class="col-md-5">
                    <div class="form-group">
                        <select name="category" class="form-control" id="BPBABAN_SELECT" style="float: left; margin-top: 10px;">

                            <?php foreach ($articles_category as $article_category):

                                if($article_category['Id'] == $category) { ?>

                                    <option value="<?php echo $article_category['Id'] ?>"> <?php echo $article_category['Name_category']?> </option>;
                                <?php }
                            endforeach; ?>

                            <?php foreach ($articles_category as $article_category):

                                if($article_category['Id'] == $category) {

                                }
                                else { ?>

                            <option value="<?php echo $article_category['Id'] ?>"> <?php echo $article_category['Name_category']?> </option>;

                            <?php }
                            endforeach; ?>


                        </select>
                    </div>
                </div>
                </td></tr>
                <tr><td><p style="float: left">Obsah:</p>
                        <div class="col-md-12">
                            <div class="form-group">
                                <textarea class="form-control textarea" rows="3" name="content" id="Content" placeholder="Content" style="float: left; margin-top: 3px; min-height: 600px"><?php echo $content; ?></textarea>
                                <script>
                                    CKEDITOR.replace( 'content' , { height: 600 } );
                                </script>
                            </div>
                        </div>
                    </td></tr>
                <tr><td><p style="float: left">Prístup:</p>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="form-group btn-group" data-toggle="buttons">
                                    <label class="btn btn-default <?php if($access == 1) echo 'active'; ?>">
                                        <input type="radio" id="app" value="1" name="access"/><span class="glyphicon glyphicon-eye-open">
                                    </label>
                                    <label class="btn btn-default <?php if($access == 0) echo 'active'; ?>">
                                        <input type="radio" id="setup" value="0" name="access"/><span class="glyphicon glyphicon-eye-close">
                                    </label>
                                </div>
                            </div>
                        </div>
                    </td></tr>
                <tr><td>
                        <div class="col-md-12">
                            <button type="submit" class="btn main-btn pull-right">Uložiť</button></a>&nbsp;&nbsp;&nbsp; </form>
                            <a href="articles" class="btn main-btn pull-right">Vrátiť sa</a>
                        </div>
                    </td></tr>

                </table>


            </div>

        </div>
    </div>
    </p>
</div>
</div>
</div>
</div>