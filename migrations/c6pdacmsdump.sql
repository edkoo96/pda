-- phpMyAdmin SQL Dump
-- version 3.3.7deb7
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 31, 2017 at 06:50 PM
-- Server version: 5.1.73
-- PHP Version: 5.3.29-1~dotdeb.0

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `c6pdacms`
--

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE IF NOT EXISTS `articles` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Category_id` int(11) NOT NULL,
  `Title` varchar(255) NOT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `Intro_image` varchar(255) DEFAULT NULL,
  `Content` text NOT NULL,
  `Keywords` varchar(255) NOT NULL,
  `Date_create` date DEFAULT NULL,
  `Visible` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`Id`),
  KEY `Category_id` (`Category_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `articles`
--

INSERT INTO `articles` (`Id`, `Category_id`, `Title`, `Description`, `Intro_image`, `Content`, `Keywords`, `Date_create`, `Visible`) VALUES
(1, 1, 'Zlučovanie galaxií', '<div class="detail-header">26.05.2017 12:14 <a hre...', NULL, '<div class="detail-header">26.05.2017 12:14 <a href="http://www.astro.cz/autori/viktoria-zemancikova.html">Vikt&oacute;ria Zemanč&iacute;kov&aacute;</a></div>\r\n\r\n<h1>Zlučovanie galaxi&iacute; zahaľuje čierne diery</h1>\r\n\r\n<p><a class="gallery-thumb" href="http://www.astro.cz/images/obrazky/original/097961.jpg" title="Zlučovanie dvoch špirálových galaxií (NGC 6050 a IC 1179), ktoré sú čoraz viac zahalené prachom a plynom.Autor: NASA, ESA, the Hubble Heritage Team (STScI/AURA)-ESA/Hubble Collaboration, and K. Noll (STScI)"><img alt="" src="http://www.astro.cz/images/clanky/velke/097961.jpg" /> </a></p>\r\n\r\n<p>Zlučovanie dvoch &scaron;pir&aacute;lov&yacute;ch galaxi&iacute; (NGC 6050 a IC 1179), ktor&eacute; s&uacute; čoraz viac zahalen&eacute; prachom a plynom.<br />\r\n<em>Autor: <a href="http://www.astronomy.com/news/2017/05/merging-galaxies-and-their-black-holes" title="NASA, ESA, the Hubble Heritage Team (STScI/AURA)-ESA/Hubble Collaboration, and K. Noll (STScI)">NASA, ESA, the Hubble Heritage Team (STScI/AURA)-ESA/Hubble Collaboration, and K. Noll (STScI)</a></em></p>\r\n\r\n<p>Vedci s použit&iacute;m teleskopu NuSTAR (NASA) dokazuj&uacute;, že v z&aacute;verenčnej f&aacute;ze zlučovania galaxi&iacute;, pad&aacute; do čiernej diery tak&eacute; množstvo plynu a prachu, ktor&eacute; je schopn&eacute; zahaliť aj akt&iacute;vne galaktick&eacute; jadr&aacute;. Kombin&aacute;cia gravitačn&yacute;ch efektov dvoch galaxi&iacute; spomaľuje r&yacute;chlosť ot&aacute;čania plynu a prachu, ktor&eacute; by v&nbsp;opačnom pr&iacute;pade voľne obiehali. T&aacute;to strata energie sp&ocirc;sobuje, že materi&aacute;l pad&aacute; do čiernej diery.</p>\r\n\r\n<div class="hlavni_obsah">\r\n<p>Čierne diery maj&uacute; v&nbsp;očiach verejnosti nedobr&uacute; povesť založen&uacute; na predpoklade, že pohltia v&scaron;etko, čo im pr&iacute;de do cesty. Av&scaron;ak v&nbsp;skutočnosti je to tro&scaron;ku inak. Okolo čiernych dier obieha prach a&nbsp;plyn a&nbsp;cel&eacute; z&aacute;stupy hviezd. A&nbsp;to až dovtedy k&yacute;m ned&ocirc;jde k&nbsp;naru&scaron;eniu tejto rovnov&aacute;hy.</p>\r\n\r\n<p>Novo publikovan&aacute; &scaron;t&uacute;dia popisuje pr&aacute;ve tak&uacute;to situ&aacute;ciu &ndash; ide o&nbsp;zl&uacute;čenie dvoch galaxi&iacute;, kedy sa k&nbsp;sebe približuj&uacute; čierne diery s&iacute;dliace v&nbsp;centre oboch galaxi&iacute;. Prach a&nbsp;plyn v&nbsp;ich okol&iacute; je tlačen&yacute; priamo do čiernej diery, pričom doch&aacute;dza k&nbsp;uvoľneniu obrovsk&eacute;ho množstva vysokoenergetick&eacute;ho žiarenia. Tento jav sa naz&yacute;va akt&iacute;vne galakt&iacute;cke jadro (AGN).</p>\r\n\r\n<p>Claudio Ricci, ved&uacute;ci autor &scaron;t&uacute;die publikovanej v <em>Monthly Notices Royal Astronomical Society, </em>tvrd&iacute;, že č&iacute;m dlh&scaron;ie trv&aacute; zlučovanie galaxi&iacute;, t&yacute;m je akt&iacute;vne jadro galaxi&iacute; zahalenej&scaron;ie. Galaxie, ktor&eacute; s&uacute; na konci procesu zlučovania, s&uacute; &uacute;plne schovan&eacute; v mračn&aacute;ch plynu a&nbsp;prachu. Ricci spolu so svojimi kolegmi pozoroval vysokoenergetick&eacute; r&ouml;ntgenov&eacute; žiarenie z 52 galaxi&iacute;, pričom asi polovica z nich sa nach&aacute;dzala v neskorej f&aacute;ze zlučovania. A pretože NuSTAR je veľmi citliv&yacute; na detekciu r&ouml;ntgenov&yacute;ch l&uacute;čov s&nbsp;veľmi vysokou energiou, bolo rozhoduj&uacute;ce, koľko svetla uniklo z oblasti plynu a prachu pokr&yacute;vaj&uacute;ceho AGN.</p>\r\n\r\n<p>&nbsp;</p>\r\n<img alt="Ilustrácia porovnáva rastúce supermasívne čierne diery v dvoch typoch galaxií. Rastúca supermassívna čierna diera v klasickej galaxii by mala okolo seba štruktúru plynu a prachu v tvare šišky (vľavo). V zlučujúcej sa galaxii materiál zahalí čiernu dieru (vpravo). Autor: National Astronomical Observatory of Japan" src="http://www.astro.cz/images/clanky/velke/097962.jpg" title="Ilustrácia porovnáva rastúce supermasívne čierne diery v dvoch typoch galaxií. Rastúca supermassívna čierna diera v klasickej galaxii by mala okolo seba štruktúru plynu a prachu v tvare šišky (vľavo). V zlučujúcej sa galaxii materiál zahalí čiernu dieru (vpravo). Autor: National Astronomical Observatory of Japan" />\r\n<p>Ilustr&aacute;cia porovn&aacute;va rast&uacute;ce supermas&iacute;vne čierne diery v dvoch typoch galaxi&iacute;. Rast&uacute;ca supermass&iacute;vna čierna diera v klasickej galaxii by mala okolo seba &scaron;trukt&uacute;ru plynu a prachu v tvare &scaron;i&scaron;ky (vľavo). V zlučuj&uacute;cej sa galaxii materi&aacute;l zahal&iacute; čiernu dieru (vpravo).<br />\r\n<em>Autor: <a href="https://phys.org/news/2017-05-merging-galaxies-enshrouded-black-holes.html" title="National Astronomical Observatory of Japan">National Astronomical Observatory of Japan</a></em></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>V&yacute;skumn&iacute;ci porovn&aacute;vaj&uacute; pozorovania galaxi&iacute; uskutočnen&yacute;ch ďalekohľadom NuSTAR &nbsp;s &uacute;dajmi NASA&#39;s Swift and Chandra and ESA&#39;s XMM-Newton observatories, ktor&eacute; sa zameriavaj&uacute; na niž&scaron;ie energetick&eacute; zložky spektra r&ouml;ntgenov&yacute;ch l&uacute;čov. Vďaka tomu vieme, že ak je v galaxii zisten&aacute; pr&iacute;tomnosť vysokoenergetick&eacute;ho r&ouml;ntgenov&eacute;ho žiarenia, av&scaron;ak žiarenie s n&iacute;zkou energiou ch&yacute;ba, znamen&aacute; to, že AGN je veľmi zahalen&eacute;.</p>\r\n\r\n<p><strong>Tieto poznatky pom&aacute;haj&uacute; potvrdiť my&scaron;lienku, že čierna diera v AGN požiera v&auml;č&scaron;inu hmoty okolo počas posledn&yacute;ch &scaron;t&aacute;di&iacute; zlučovania galaxi&iacute;</strong>. Vtedy doch&aacute;dza k&nbsp;najv&auml;č&scaron;iemu rastu supermas&iacute;vnej čiernej diery, vďaka čomu vieme lep&scaron;ie porozumieť vzťahu medzi čiernou dierou a&nbsp;jej hostiteľskou galaxiou.</p>\r\n\r\n<p>Zdroje a doporučen&eacute; odkazy:<br />\r\n[1] <a href="https://phys.org/news/2017-05-merging-galaxies-enshrouded-black-holes.html">phys.org</a><br />\r\n[2] <a href="https://www.nasa.gov/feature/jpl/merging-galaxies-have-enshrouded-black-holes">nasa.gov</a><br />\r\n[3] <a href="https://www.jpl.nasa.gov/news/news.php?release=2017-136">jpl.nava.gov</a></p>\r\n</div>\r\n', '', '2017-05-10', 1),
(2, 1, 'Supermasivní černá díra', '<div class="detail-header">24.05.2017 06:48 <a hre...', '', '<div class="detail-header">24.05.2017 06:48 <a href="http://www.astro.cz/autori/frantisek-martinek.html">Franti&scaron;ek Martinek</a></div>\r\n\r\n<h1>Supermasivn&iacute; čern&aacute; d&iacute;ra byla vyvržena při galaktick&eacute; kolizi</h1>\r\n\r\n<p><a class="gallery-thumb" href="http://www.astro.cz/images/obrazky/original/098032.jpg" title="Supermasivní černá díra unikající z galaxieAutor: NASA/CXC/M.Weiss"><img alt="" src="http://www.astro.cz/images/clanky/velke/098032.jpg" /> </a></p>\r\n\r\n<p>Supermasivn&iacute; čern&aacute; d&iacute;ra unikaj&iacute;c&iacute; z galaxie<br />\r\n<em>Autor: NASA/CXC/M.Weiss</em></p>\r\n\r\n<p>Supermasivn&iacute; čern&eacute; d&iacute;ry jsou v&scaron;eobecně stacion&aacute;rn&iacute;mi objekty, kter&eacute; s&iacute;dl&iacute; v&nbsp;centrech vět&scaron;iny velk&yacute;ch galaxi&iacute;. Nicm&eacute;ně na z&aacute;kladě použit&iacute; dat z&nbsp;rentgenov&eacute; družice NASA s&nbsp;n&aacute;zvem Chandra X-ray Observatory a z&nbsp;dal&scaron;&iacute;ch teleskopů astronomov&eacute; ned&aacute;no vyp&aacute;trali, že může existovat supermasivn&iacute; čern&aacute; d&iacute;ra, kter&aacute; se pohybuje. Tato možn&aacute; čern&aacute; d&iacute;ra &ndash; odpadl&iacute;k, kter&aacute; m&aacute; hmotnost přibližně 160 mili&oacute;nů hmotnost&iacute; Slunce, se nach&aacute;z&iacute; v&nbsp;eliptick&eacute; galaxii vzd&aacute;len&eacute; zhruba 3,9 miliardy světeln&yacute;ch roků od Země. Astronomov&eacute; maj&iacute; z&aacute;jem o tuto pohybuj&iacute;c&iacute; se supermasivn&iacute; černou d&iacute;ru, protože tak mohou odhalit v&iacute;ce informac&iacute; o vlastnostech těchto tajemn&yacute;ch objektů.</p>\r\n\r\n<div class="hlavni_obsah">\r\n<p>Když se dvě men&scaron;&iacute; supermasivn&iacute; čern&eacute; d&iacute;ry sraz&iacute;, splynou za vytvořen&iacute; vět&scaron;&iacute;ho objektu. Tato kolize by měla z&aacute;roveň generovat gravitačn&iacute; vlny, kter&eacute; jsou emitov&aacute;ny mnohem silněji v&nbsp;jednom směru než v&nbsp;jin&yacute;ch. Nově vytvořen&aacute; čern&aacute; d&iacute;ra tak může obdržet &bdquo;kopanec&ldquo; v&nbsp;opačn&eacute;m směru, než směřuj&iacute; tyto intenzivn&iacute; gravitačn&iacute; vlny. Tento impuls může vyhodit černou d&iacute;ru z&nbsp;centra galaxie, jak je zn&aacute;zorněno na ilustraci.</p>\r\n\r\n<p>S&iacute;la impulsu z&aacute;vis&iacute; na rychlosti a směru rotace obou men&scaron;&iacute;ch čern&yacute;ch děr před jejich splynut&iacute;m. Z&nbsp;tohoto důvodu informace o těchto důležit&yacute;ch, av&scaron;ak těžko postižiteln&yacute;ch vlastnostech, mohou b&yacute;t z&iacute;sk&aacute;ny na z&aacute;kladě studia rychlosti pohybu čern&yacute;ch děr.</p>\r\n\r\n<p>Astronomov&eacute; objevili tyto kandid&aacute;ty na čern&eacute; d&iacute;ry na z&aacute;kladě studia rentgenov&eacute;ho z&aacute;řen&iacute; a optick&yacute;ch dat pro tis&iacute;ce galaxi&iacute;. Zaprv&eacute; použili pozorov&aacute;n&iacute; družice Chandra k&nbsp;v&yacute;běru galaxi&iacute;, kter&eacute; obsahuj&iacute; jasn&eacute; zdroje rentgenov&eacute;ho z&aacute;řen&iacute; a kter&eacute; byly pozorov&aacute;ny jako souč&aacute;st přehl&iacute;dky Sloan Digital Sky Survey (<strong>SDSS</strong>). Jasn&aacute; rentgenov&aacute; emise je obvykl&yacute;m rysem supermasivn&iacute;ch čern&yacute;ch děr, kter&eacute; velmi rychle zvět&scaron;uj&iacute; svoji velikost.</p>\r\n\r\n<p>D&aacute;le se vědci pod&iacute;vali, jestli pozorov&aacute;n&iacute; jasn&yacute;ch rentgenov&yacute;ch galaxi&iacute; pomoc&iacute; Hubbleova kosmick&eacute;ho teleskopu <strong>HST</strong> odhal&iacute; dvě maxima pobl&iacute;ž jejich středu na sn&iacute;mc&iacute;ch v&nbsp;optick&eacute;m oboru. Tato dvě maxima mohou ukazovat, že je zde př&iacute;tomna dvojice supermasivn&iacute;ch čern&yacute;ch děr, nebo že se odvržen&aacute; čern&aacute; d&iacute;ra pohybuje pryč od seskupen&iacute; hvězd v&nbsp;centru dan&eacute; galaxie.</p>\r\n\r\n<p>Pokud byla krit&eacute;ria splněna, pak astronomov&eacute; prozkoumali spektra SDSS, kter&aacute; uk&aacute;zala, jak velk&eacute; množstv&iacute; viditeln&eacute;ho světla kol&iacute;s&aacute; s&nbsp;jeho vlnovou d&eacute;lkou. Jestliže astronomov&eacute; objevili o mnoh&eacute;m vypov&iacute;daj&iacute;c&iacute; charakteristick&eacute; rysy ve spektru, naznačuj&iacute;c&iacute; př&iacute;tomnost supermasivn&iacute; čern&eacute; d&iacute;ry, n&aacute;sledoval je&scaron;tě bliž&scaron;&iacute; průzkum těchto galaxi&iacute;.</p>\r\n\r\n<p>&nbsp;</p>\r\n<a class="gallery-thumb" href="http://www.astro.cz/images/obrazky/original/098033.jpg" title="Supermasivní černá díra unikající z galaxie Autor: X-ray: NASA/CXC/NRAO/D.-C.Kim; Optical: NASA/STScI; Illustration: NASA/CXC/M.Weiss"><img alt="Supermasivní černá díra unikající z galaxie Autor: X-ray: NASA/CXC/NRAO/D.-C.Kim; Optical: NASA/STScI; Illustration: NASA/CXC/M.Weiss" src="http://www.astro.cz/images/clanky/velke/098033.jpg" title="Supermasivní černá díra unikající z galaxie Autor: X-ray: NASA/CXC/NRAO/D.-C.Kim; Optical: NASA/STScI; Illustration: NASA/CXC/M.Weiss" /></a>\r\n\r\n<p>Supermasivn&iacute; čern&aacute; d&iacute;ra unikaj&iacute;c&iacute; z galaxie<br />\r\n<em>Autor: X-ray: NASA/CXC/NRAO/D.-C.Kim; Optical: NASA/STScI; Illustration: NASA/CXC/M.Weiss</em></p>\r\nPo v&scaron;ech těchto p&aacute;tr&aacute;n&iacute;ch po vhodn&eacute;m kandid&aacute;tu byla prchaj&iacute;c&iacute; čern&aacute; d&iacute;ra konečně objevena. Lev&yacute; sn&iacute;mek na vložen&eacute;m obr&aacute;zku poř&iacute;zen&yacute; pomoc&iacute; HST ukazuje dvě jasn&eacute; skvrny pobl&iacute;ž středu galaxie. Jedna z&nbsp;nich se nach&aacute;z&iacute; přesně uprostřed galaxie, druh&aacute; <strong>je vzd&aacute;lena asi 3&nbsp;000 světeln&yacute;ch roků od jej&iacute;ho centra</strong>. Pozděj&scaron;&iacute; prameny ukazuj&iacute; vlastnosti rostouc&iacute; superhmotn&eacute; čern&eacute; d&iacute;ry a jej&iacute; polohu shoduj&iacute;c&iacute; se s&nbsp;jasn&yacute;m zdrojem rentgenov&eacute;ho z&aacute;řen&iacute;, kter&yacute; byl objeven družic&iacute; Chandra (viz prav&yacute; sn&iacute;mek na vložen&eacute;m obr&aacute;zku). Na z&aacute;kladě použit&iacute; dat z&nbsp;přehl&iacute;dky SDSS a z&nbsp;pozorov&aacute;n&iacute; pomoc&iacute; dalekohledu Keck na Havajsk&yacute;ch ostrovech astronomov&eacute; určili, že rostouc&iacute; čern&aacute; d&iacute;ra se nach&aacute;z&iacute; pobl&iacute;ž, av&scaron;ak viditelně mimo střed a m&aacute; rychlost, kter&aacute; je odli&scaron;n&aacute; od pohybu galaxie. Z&nbsp;to vypl&yacute;v&aacute;, že tento zdroj může b&yacute;t unikaj&iacute;c&iacute; superhmotn&aacute; čern&aacute; d&iacute;ra.\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Mateřsk&aacute; galaxie unikaj&iacute;c&iacute; čern&eacute; d&iacute;ry rovněž vykazuje určit&eacute; zn&aacute;mky ru&scaron;iv&yacute;ch podnětů v&nbsp;jej&iacute;ch vněj&scaron;&iacute;ch oblastech, z kter&yacute;ch vypl&yacute;v&aacute;, že ke splynut&iacute; dvou galaxi&iacute; do&scaron;lo relativně ned&aacute;vno. Protože ke splynut&iacute; supermasivn&iacute;ch čern&yacute;ch děr zřejmě doch&aacute;z&iacute;, když se sraz&iacute; jejich mateřsk&eacute; galaxie, tato informace podporuje představu o prchaj&iacute;c&iacute; supermasivn&iacute; čern&eacute; d&iacute;ře v&nbsp;tomto syst&eacute;mu.</p>\r\n\r\n<p>Kromě toho hvězdy vznikaj&iacute; v galaxii vysok&yacute;m tempem několik set hmotnost&iacute; Slunce za rok. To souhlas&iacute; s&nbsp;poč&iacute;tačov&yacute;mi simulacemi, kter&eacute; předpokl&aacute;daj&iacute;, že rychlost vzniku hvězd může b&yacute;t zes&iacute;lena splynut&iacute;m galaxi&iacute;, a to zejm&eacute;na v&nbsp;př&iacute;padě, kdy obsahuj&iacute; vyvrženou černou d&iacute;ru.</p>\r\n\r\n<p>Dal&scaron;&iacute; možn&eacute; vysvětlen&iacute; z&iacute;skan&yacute;ch dat je, že obě supermasivn&iacute; čern&eacute; d&iacute;ry se nach&aacute;zej&iacute; v&nbsp;centru galaxie v&nbsp;souhvězd&iacute; Velk&eacute; medvědice, av&scaron;ak jedna z&nbsp;nich nevyzařuje zjistiteln&eacute; z&aacute;řen&iacute;, protože zvět&scaron;uje svoji velikost př&iacute;li&scaron; pomalu. Vědci upřednostňuj&iacute; vysvětlen&iacute; na z&aacute;kladě prchaj&iacute;c&iacute; čern&eacute; d&iacute;ry, av&scaron;ak potřebuj&iacute; je&scaron;tě dal&scaron;&iacute; data k&nbsp;pos&iacute;len&iacute; tohoto předpokladu.</p>\r\n\r\n<p>Zdroje a doporučen&eacute; odkazy:<br />\r\n[1] <a href="https://phys.org/news/2017-05-astronomers-pursue-renegade-supermassive-black.html">phys.org</a><br />\r\n[2] <a href="http://chandra.harvard.edu/photo/2017/rsmbh/">chandra.harvard.edu</a><br />\r\n[3] <a href="https://www.universetoday.com/135539/astronomers-find-rogue-supermassive-black-hole-kicked-galactic-collision/">universetoday.com</a></p>\r\n</div>\r\n', '', '2017-05-30', 0),
(3, 2, 'Je reálne, aby po roku 2020 mala internet každá obec', '<h1>Je re&aacute;lne, aby po roku 2020 mala intern...', '', '<h1>Je re&aacute;lne, aby po roku 2020 mala internet každ&aacute; obec</h1>\r\n\r\n<p>22. 1. 2017 Spravodajstvo</p>\r\n\r\n<div class="image-holder"><img alt="" src="https://www.istp.sk/clanok/12094/clanky_ilustracie/12094-articleheadline.jpg" /></div>\r\n\r\n<div class="body">\r\n<div class="share-content pinned">&nbsp;</div>\r\n\r\n<div class="wrapper"><strong>Bratislava 22. janu&aacute;ra (TASR)</strong> &ndash; Je re&aacute;lne, aby sme po roku 2020 mohli skon&scaron;tatovať, že z každ&eacute;ho k&uacute;ta Slovenska a z každej obce je možn&eacute; sa pripojiť na internet. Uviedol to podpredseda vl&aacute;dy SR pre invest&iacute;cie a informatiz&aacute;ciu Peter Pellegrini (Smer-SD) v dne&scaron;nej diskusnej rel&aacute;cii telev&iacute;zie TA3 V politike. Už v tomto roku by sa totiž mal podľa neho spustiť projekt tzv. odstraňovania bielych miest, čiže dotiahnutie mobiln&eacute;ho sign&aacute;lu a vysokor&yacute;chlostn&eacute;ho internetov&eacute;ho pripojenia do každej obce a mesta na Slovensku, ktor&yacute; bude financovan&yacute; z eurofondov.<br />\r\n<br />\r\n<em>&quot;Pr&iacute;stup k modern&yacute;m technol&oacute;gi&aacute;m a inform&aacute;ci&aacute;m by mal mať každ&yacute;, bez ohľadu na to, v ktorej oblasti Slovenska žije a bez ohľadu na to, z ak&eacute;ho soci&aacute;lneho prostredia poch&aacute;dza,</em>&quot; povedal Pellegrini. Je totiž presvedčen&yacute;, že ak to nedok&aacute;žu telekomunikačn&iacute; oper&aacute;tori, ktor&iacute; podľa neho investuj&uacute; iba tam, kde z toho m&ocirc;žu profitovať, je to povinnosťou &scaron;t&aacute;tu. V tejto s&uacute;vislosti z&aacute;roveň ver&iacute;, že oper&aacute;tori nebud&uacute; kl&aacute;sť pri sp&uacute;&scaron;ťan&iacute; projektu žiadne prek&aacute;žky.<br />\r\n<br />\r\nRok 2017 by v&scaron;ak mal byť podľa Pellegriniho aj rokom, keď sa vl&aacute;da bude viac s&uacute;streďovať na t&yacute;ch, ktor&iacute; pracuj&uacute;. Im by sa mali predov&scaron;etk&yacute;m zlep&scaron;ovať mzdov&eacute; podmienky, preto je vyv&iacute;jan&yacute; v&yacute;razn&yacute; tlak na r&yacute;chlej&scaron;ie zv&yacute;&scaron;enie minim&aacute;lnej mzdy, ktor&aacute; by mala byť čo najsk&ocirc;r viac ako 500 eur.<em> &quot;Budeme sa venovať viac t&yacute;m, ktor&iacute; pracuj&uacute; a budeme sa musieť tvrd&scaron;ie vysporiadať s t&yacute;mi, ktor&iacute; pracovať nechc&uacute;. Teda, ktor&yacute;m je aj pr&aacute;ca pon&uacute;kan&aacute;, ale zneuž&iacute;vaj&uacute; soci&aacute;lny syst&eacute;m. Mus&iacute; byť jasne c&iacute;tiť, že ak niekto nechce pracovať a odmieta pon&uacute;kan&uacute; pr&aacute;cu, mus&iacute; sa mať v&yacute;razne hor&scaron;ie ako niekto, kto ide pracovať, aj keď za minim&aacute;lnu mzdu,&quot;</em> vysvetľuje.<br />\r\n<br />\r\nV neposlednom rade by malo &iacute;sť tiež o znižovanie region&aacute;lnych rozdielov na &uacute;zem&iacute; Slovenska. V tejto s&uacute;vislosti by mal byť tento rok podľa Pellegriniho rokom plnenia a realiz&aacute;cie pl&aacute;nov rozvoja jednotliv&yacute;ch okresov, ktor&eacute; boli v minulosti schv&aacute;len&eacute;. &quot;<em>Nie s&uacute; to fantazmag&oacute;rie &uacute;radn&iacute;kov z ministerstiev, to si tvorili priamo občania, firmy, samospr&aacute;vy, okresn&eacute; &uacute;rady,&quot;</em> hovor&iacute; s t&yacute;m, že gro nov&yacute;ch pracovn&yacute;ch miest, najm&auml; v regi&oacute;noch, ktor&eacute; to najviac potrebuj&uacute;, by malo vznikn&uacute;ť v tomto roku a začiatkom roka 2018. S&uacute;vis&iacute; s t&yacute;m aj prechod region&aacute;lneho rozvoja pod &Uacute;rad vl&aacute;dy SR a zintenz&iacute;vnenie spolupr&aacute;ce so &scaron;kolami a zamestn&aacute;vateľmi v jednotliv&yacute;ch krajoch.</div>\r\n</div>\r\n', '', '2017-05-29', 1),
(4, 3, 'Lorem Ipsum', '<p>Lorem ipsum dolor sit amet, consectetur adipisc...', '', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras efficitur lacinia neque eget tempor. Vivamus porttitor interdum eros at blandit. Pellentesque turpis velit, imperdiet nec nibh vitae, dignissim luctus risus. Phasellus massa libero, faucibus non cursus nec, commodo quis arcu. In in enim nibh. Aenean sit amet eleifend eros, non luctus orci. Praesent mollis ornare enim non ornare. Sed posuere sapien quis egestas imperdiet. Ut eget gravida odio. Proin ultrices iaculis sapien vitae cursus. Integer quis ligula ex. Cras a feugiat ante, vulputate mattis ipsum. Sed libero ipsum, tristique iaculis scelerisque vel, dignissim at metus.</p>\r\n\r\n<p>Suspendisse potenti. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aenean non elementum tellus, quis efficitur nulla. Aenean finibus placerat felis, at accumsan quam pharetra id. Suspendisse quis ante a dui dictum facilisis et quis est. Mauris vehicula ligula nisi, at finibus metus iaculis et. Nunc tristique scelerisque elit vel viverra. Vestibulum maximus, erat interdum auctor volutpat, lacus metus eleifend nisl, dignissim lobortis velit tellus vel felis. Aenean sagittis nisi in dictum vehicula. Fusce sem nulla, suscipit ut metus non, ultricies finibus elit. Ut consectetur, risus sit amet efficitur dapibus, nisi nibh dapibus odio, eget scelerisque libero nulla fringilla erat.</p>\r\n\r\n<p>Donec in elit sapien. Curabitur porttitor viverra lacus et egestas. Fusce facilisis nulla diam, eu porttitor velit euismod nec. Sed pulvinar, lorem ac viverra bibendum, magna erat pharetra elit, at porta odio nibh nec lorem. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed eleifend non sapien vel laoreet. Praesent metus orci, aliquet at nisi vehicula, hendrerit elementum mi. Proin id dolor at libero vestibulum tristique. Ut tempus ipsum non efficitur eleifend.</p>\r\n', '', '0000-00-00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `articles_category`
--

CREATE TABLE IF NOT EXISTS `articles_category` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name_category` varchar(30) NOT NULL,
  `Visible` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `articles_category`
--

INSERT INTO `articles_category` (`Id`, `Name_category`, `Visible`) VALUES
(1, 'Vesmír', 1),
(2, 'Internet', 1),
(3, 'Iné', 1);

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE IF NOT EXISTS `menu` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) NOT NULL,
  `Chart` int(11) DEFAULT NULL,
  `Content` text NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`Id`, `Name`, `Chart`, `Content`) VALUES
(1, 'Domov', 1, '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras efficitur lacinia neque eget tempor. Vivamus porttitor interdum eros at blandit. Pellentesque turpis velit, imperdiet nec nibh vitae, dignissim luctus risus. Phasellus massa libero, faucibus non cursus nec, commodo quis arcu. In in enim nibh. Aenean sit amet eleifend eros, non luctus orci. Praesent mollis ornare enim non ornare. Sed posuere sapien quis egestas imperdiet. Ut eget gravida odio. Proin ultrices iaculis sapien vitae cursus. Integer quis ligula ex. Cras a feugiat ante, vulputate mattis ipsum. Sed libero ipsum, tristique iaculis scelerisque vel, dignissim at metus.</p>\r\n\r\n<p>Suspendisse potenti. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aenean non elementum tellus, quis efficitur nulla. Aenean finibus placerat felis, at accumsan quam pharetra id. Suspendisse quis ante a dui dictum facilisis et quis est. Mauris vehicula ligula nisi, at finibus metus iaculis et. Nunc tristique scelerisque elit vel viverra. Vestibulum maximus, erat interdum auctor volutpat, lacus metus eleifend nisl, dignissim lobortis velit tellus vel felis. Aenean sagittis nisi in dictum vehicula. Fusce sem nulla, suscipit ut metus non, ultricies finibus elit. Ut consectetur, risus sit amet efficitur dapibus, nisi nibh dapibus odio, eget scelerisque libero nulla fringilla erat.</p>\r\n'),
(2, 'O nás', NULL, 'orem ipsum dolor sit amet, consectetur adipiscing elit. Cras efficitur lacinia neque eget tempor. Vivamus porttitor interdum eros at blandit. Pellentesque turpis velit, imperdiet nec nibh vitae, dignissim luctus risus. Phasellus massa libero, faucibus non cursus nec, commodo quis arcu. In in enim nibh. Aenean sit amet eleifend eros, non luctus orci. Praesent mollis ornare enim non ornare. Sed posuere sapien quis egestas imperdiet. Ut eget gravida odio. Proin ultrices iaculis sapien vitae cursus. Integer quis ligula ex. Cras a feugiat ante, vulputate mattis ipsum. Sed libero ipsum, tristique iaculis scelerisque vel, dignissim at metus.\r\n\r\nSuspendisse potenti. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aenean non elementum tellus, quis efficitur nulla. Aenean finibus placerat felis, at accumsan quam pharetra id. Suspendisse quis ante a dui dictum facilisis et quis est. Mauris vehicula ligula nisi, at finibus metus iaculis et. Nunc tristique scelerisque elit vel viverra. Vestibulum maximus, erat interdum auctor volutpat, lacus metus eleifend nisl, dignissim lobortis velit tellus vel felis. Aenean sagittis nisi in dictum vehicula. Fusce sem nulla, suscipit ut metus non, ultricies finibus elit. Ut consectetur, risus sit amet efficitur dapibus, nisi nibh dapibus odio, eget scelerisque libero nulla fringilla erat.');

-- --------------------------------------------------------

--
-- Table structure for table `photo`
--

CREATE TABLE IF NOT EXISTS `photo` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Category_id` int(11) NOT NULL,
  `Path_image` varchar(255) NOT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `Keywords` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `Category_id` (`Category_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;


-- --------------------------------------------------------

--
-- Table structure for table `photo_category`
--

CREATE TABLE IF NOT EXISTS `photo_category` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) NOT NULL,
  `Visible` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `photo_category`
--

INSERT INTO `photo_category` (`Id`, `Name`, `Visible`) VALUES
(1, 'Príroda', 1),
(2, 'Folklór', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(30) NOT NULL,
  `Surname` varchar(30) DEFAULT NULL,
  `Username` varchar(20) NOT NULL,
  `Password` varchar(32) NOT NULL,
  `Email` varchar(255) NOT NULL,
  `Level` enum('superadmin','admin','member') DEFAULT NULL,
  `Date_create` date DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`Id`, `Name`, `Surname`, `Username`, `Password`, `Email`, `Level`, `Date_create`) VALUES
(1, 'Marián', 'Melich', 'm_melich', '955db0b81ef1989b4a4dfeae8061a9a6', 'marian.melich.mm@gmail.com', 'superadmin', '2017-05-12'),
(2, 'Jožko', 'Mrkvička', 'mrvo', 'b7dd20d4131efa3b00700ada478f1034', 'mrkvo@gmail.com', 'member', '2017-05-31'),
(3, 'Admin', 'Admin', 'admin', '955db0b81ef1989b4a4dfeae8061a9a6', 'admincms@gmail.com', 'superadmin', '2017-05-20');
